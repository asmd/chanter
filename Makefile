chanter: chanter.c
	cc chanter.c -O3 -o chanter
	strip chanter

install: chanter
	cp chanter ~/bin/

uninstall:
	rm -f ~/bin/chanter

clean:
	rm -f chanter
