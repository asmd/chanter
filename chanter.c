#define	MIN_ARGC	3
#define UINT_LENGTH	10
#define CHAR_LENGTH	3
#define	LOG_ITEM_SIZE	8

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <sys/time.h>

void usage(int n) {
	dprintf(2, "chanter <blocksize> <count> <fill> [ <logfile> ]\nerror: %i\n", n);
	exit(n);
}
unsigned int str2uint(unsigned char *p) {
	unsigned long int res = 0;
	size_t len = strnlen(p, UINT_LENGTH + 1);
	if (len == 0) usage(2);
	if (len > UINT_LENGTH) usage(3);
	for (int i = 0; i < len; i++) {
		if (p[i] < '0' || p[i] > '9') usage(4);
		res *= 10; res += p[i] - '0';
	}
	if (res > 0xFFFFFFFF) usage(5);
	if (res == 0) usage(6);
	return (unsigned int) res;
}
unsigned char str2ubyte(unsigned char *p) {
	unsigned short res = 0;
	size_t len = strnlen(p, CHAR_LENGTH + 1);
	if (len == 0) usage(7);
	if (len > CHAR_LENGTH) usage(8);
	for (int i = 0; i < len; i++) {
		if (p[i] < '0' || p[i] > '9') usage(9);
		res *= 10; res += p[i] - '0';
	}
	if (res > 0xFF) usage(10);
	return (unsigned char) res;
}
int main(int argc, char *const *argv) {
	if ( argc <= MIN_ARGC ) usage(1);
	unsigned int block_size = str2uint(argv[1]);
	unsigned int block_count = str2uint(argv[2]);
	unsigned char block_fill = str2ubyte(argv[3]);
	unsigned char *pattern = malloc(block_size);
	if (pattern == NULL) usage(13);
	for (int i = 0; i < block_size; i++) pattern[i] = block_fill;
	int file_log = 0;
	void* log = NULL;
	struct timeval time_before, time_after, time_diff, *log_cursor;
	if ( argc > MIN_ARGC + 1 ) {
		log = malloc(block_count * LOG_ITEM_SIZE);
		if (log == NULL) usage(16);
		log_cursor = log;
		file_log = open(argv[4], O_CREAT|O_WRONLY|O_TRUNC, S_IRUSR);
		if (file_log <= 0) usage(12);
	}
	for (int i = 0; i < block_count; i++) {
		ssize_t s = block_size;
		if (log != NULL) if (gettimeofday(&time_before, NULL) != 0) usage(17);
		do {
			ssize_t rd = write(1, pattern, s);
			if (rd > 0) { s -= rd; }
			else if (rd < 0) { usage(14); }
			else usage(15);
		} while ( s > 0 );
		if (log != NULL) {
			if (gettimeofday(&time_after, NULL) != 0) usage(17);
			if (time_after.tv_usec > time_before.tv_usec) {
				time_diff.tv_sec = time_after.tv_sec - time_before.tv_sec;
				time_diff.tv_usec = time_after.tv_usec - time_before.tv_usec;
			} else {
				time_diff.tv_sec = time_after.tv_sec - time_before.tv_sec - 1;
				time_diff.tv_usec = 1000000 + time_after.tv_usec - time_before.tv_usec;
			}
			memcpy(log_cursor, &time_diff, LOG_ITEM_SIZE);
			log_cursor++;
		}
	}
	if (log != NULL) {
		ssize_t s = block_count * LOG_ITEM_SIZE;
		do {
			ssize_t rd = write(file_log, log, s);
			if (rd > 0) { s -= rd; }
			else if (rd < 0) { usage(18); }
			else usage(19);
		} while ( s > 0 );
		free(log);
		close(file_log);
	}
	free(pattern);
}
